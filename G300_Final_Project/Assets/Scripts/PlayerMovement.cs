﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public float moveSpeed;
    private float maxSpeed = 5f;
    private Vector3 input;

    private Vector3 spawn;

    // Use this for initialization
    void Start()
    {
        spawn = transform.position;
    }


    void Update()
    {
        input = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        if (GetComponent<Rigidbody>().velocity.magnitude < maxSpeed)
        {
            GetComponent<Rigidbody>().AddForce(input * moveSpeed);
        }
    }

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Pick Up")) 
		{
			other.gameObject.SetActive (false);
		}
	}


    void OnCollisionEnter(Collision other)
    {
        if (other.transform.tag == "Enemy")
        {
            transform.position = spawn;
        }
    }
}
